// eslint-disable-next-line no-unused-vars
function main(user, context, callback) {
  user.user_metadata = user.user_metadata || {};
  user.app_metadata = user.app_metadata || {};

  //Automatically parse json response bodies from the request-promise library
  var rp = require('request-promise').defaults({ json: true });
  var Promise = require('bluebird@3.4.6');

  //auth0.baseUrl contains the ManagementAPI endpoint, we want just the auth0 domain url
  var tokenUrl = auth0.baseUrl.substring(0, auth0.baseUrl.indexOf('/api/v2')) + '/oauth/token';
  var client_id = configuration.AUTH0_CLIENT_ID;
  var client_secret = configuration.AUTH0_CLIENT_SECRET;

  //environment specific url for traena api
  var traenaBaseUrl = configuration.TRAENA_BASE_URL;
  var traenaUsersEndpoint = traenaBaseUrl + '/internal/users/' + user.email;

  var authenticationApiOpts = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    form: {
      grant_type: 'client_credentials',
      client_id: client_id,
      client_secret: client_secret,
      // TODO swap to https://oidc.traena.io after switching internal endpoints in api over to RS256
      audience: 'https://traena.io',
    },
    url: tokenUrl,
    json: true,
  };

  /**
   * Get an Auth0 client-credentials authentication token for the traena api
   */
  function getAuth0Token() {
    //TODO: Optimize using globals to not call for EVERY login
    console.log("Client Credentials Endpoint: " + tokenUrl);
    return rp(authenticationApiOpts).then(function (parsedBody) {
      return parsedBody.access_token;
    }).catch(function (error) {
      console.log("Error getting Client Credentials Token! " + error);
      callback(error);
    });
  }

  function resolveToken() {
    if (global.client_credentials_token) {
      return Promise.resolve(global.client_credentials_token);
    } else {
      return getAuth0Token();
    }
  }

  // TODO: Remove this after we are fully migrated to OIDC compliant endpoints
  function injectLegacyClaims() {
    user.details = {
      traenaId: user.traenaId,
      organizationId: user.organizationId,
      firstName: user.firstName,
      lastName: user.lastName,
      profileImageURI: user.profileImageURI,
      subscribedChannelIds: user.subscribedChannelIds,
    };
  }

  /**
   * Inject user detail information into ID and Access token using OIDC compliant (namespaced) claims
   */
  function injectOidcClaims(traenaUser) {
    var userNamespace = 'https://traena.io/user';


    // Do nothing on refresh token
    if (context.protocol === 'delegation') {
      return;
    }

    var details = {
      traenaId: traenaUser.id,
      organizationId: traenaUser.organizationId,
      organizationName: traenaUser.organization && traenaUser.organization.name,
      firstName: traenaUser.firstName,
      lastName: traenaUser.lastName,
      profileImageURI: (traenaUser.profileImageURI) ? traenaUser.profileImageURI : undefined,
      subscribedChannelIds: traenaUser.subscribedChannelIds,
    };

    context.idToken[userNamespace] = {
      email: traenaUser.email,
      details: details,
      groups: user.groups,
      roles: user.roles,
      permissions: user.permissions
    };

    // OIDC compliant approach to injecting values into access token
    context.accessToken[userNamespace] = {
      email: traenaUser.email,
      details: details,
      groups: user.groups,
      roles: user.roles,
      permissions: user.permissions
    };
  }

  //store data back to auth0 user
  function updateAuth0User(traenaUser) {

    //Add to the user's persisted app-specific profile attributes
    Object.assign(user.app_metadata, {
      traenaId: traenaUser.id,
      organizationId: traenaUser.organizationId,
    });

    //Add to the user's persisted user-specific profile attributes
    Object.assign(user.user_metadata, {
      firstName: traenaUser.firstName,
      lastName: traenaUser.lastName,
      picture: traenaUser.profileImageURI,
      position: traenaUser.position,
    });

    //Add to the transient user so that data is accessible to any subsequent rules
    Object.assign(user, {
      traenaId: traenaUser.id,
      organizationId: traenaUser.organizationId,
      organizationName: traenaUser.organization && traenaUser.organization.name,
      firstName: traenaUser.firstName,
      lastName: traenaUser.lastName,
    });

    injectLegacyClaims(traenaUser);
    injectOidcClaims(traenaUser);

    var updateUserMetadataPromise = auth0.users.updateUserMetadata(user.user_id, user.user_metadata);
    var updateAppMetadataPromise = auth0.users.updateAppMetadata(user.user_id, user.app_metadata);

    return q.all(updateUserMetadataPromise, updateAppMetadataPromise);
  }

  function getTraenaDbUser(token) {
    console.log("Traena Endpoint: " + traenaUsersEndpoint);

    var traenaApiOpts = {
      headers: {
        Authorization: 'Bearer ' + token
      },
      url: traenaUsersEndpoint,
    };

    return rp.get(traenaApiOpts).then(function (parsedBody) {
      console.log("Traena User Info: " + JSON.stringify(parsedBody));
      return parsedBody;
    }).catch(function (error) {
      console.log("Error getting Traena DB User! " + error);
      callback(error);
    });
  }

  resolveToken()
  .then(getTraenaDbUser)
  .then(updateAuth0User)

  //All done! Invoke Auth0 Rules callback
  .then(function () {
    //For some reason this has to be in a function or the user data is not set properly
    callback(null, user, context);
  })
  .catch(function (error) {
    console.log("Error! " + error);
    callback(error);
  });
}
