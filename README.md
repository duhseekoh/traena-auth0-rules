# Auth0 Rules

Repository to store rules and hosted pages for deploying to auth0.

Rules are hooks into the authentication process that allow us to execute arbitrary javascript code during the authentication flow.

Hosted Pages are pages in which end users will be seeing, and are hosted at the auth0.com domain (login, password reset).

## Deployment

These rules are deployed using https://auth0.com/docs/extensions/github-deploy.

Any changes to the develop branch of this repo, when pushed, will be applied to the rules at the traena-dev auth0 environment.

- `branch-name` -> `auth0-account-name`
- `develop` -> `traena-dev`
- `staging` -> `traena-staging`
- `master` -> `traena-production`

### Rules
If you add a new rule to the `./rules` directory it will automatically be deployed on the next push.

### Hosted Pages
Hosted pages can be added to the `./pages` directory. Any hosted pages we intend to modify should be stored in this repo.
Deployable pages are limited to the pages listed here: https://auth0.com/docs/extensions/github-deploy#deploy-hosted-pages

## Rule Environment variables

Rules need access to environment specific variables. Auth0 calls these `configuration` variables. When deploying to a new
environment, make sure to set these variables up in the auth0 UI. These are listed at the bottom of the rules screen https://manage.auth0.com/#/rules

- TRAENA_BASE_URL: api url e.g. https://dev.api-traena.io
- AUTH0_CLIENT_ID: get from auth0 "Auth0 Rules (Rules to Traena API)" client
- AUTH0_CLIENT_SECRET: get from auth0 "Auth0 Rules (Rules to Traena API)" client

## Manual rules

Rules in auth0 can be marked as manual, meaning they won't get deleted or updated from this repo. We should shoot for managing all rules
within this repo.
https://auth0.com/docs/extensions/github-deploy#deploy-rules
